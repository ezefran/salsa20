#include <gtest/gtest.h>
#include <Salsa20.h>
#include <Utils.h>

/* Casos extraídos de la especificación de Salsa20 */

TEST(Salsa20_quarterRound, 001) {
    uint32_t* z;
    uint32_t y[4] = {0, 0, 0, 0};
    uint32_t expected[4] = {0, 0, 0, 0};
    z = Salsa20::quarterRound(y);
    
    for (int i = 0; i < 4; i++) {
        ASSERT_EQ(expected[i], z[i]);
    }
    
    delete[] z;
}

TEST(Salsa20_quarterRound, 002) {
    uint32_t* z;
    uint32_t y[4] = {1, 0, 0, 0};
    uint32_t expected[4] = {0x08008145, 0x00000080, 0x00010200, 0x20500000};
    z = Salsa20::quarterRound(y);
    
    for (int i = 0; i < 4; i++) {
        ASSERT_EQ(expected[i], z[i]);
    }
    
    delete[] z;
}

TEST(Salsa20_quarterRound, 003) {
    uint32_t* z;
    uint32_t y[4] = {0, 1, 0, 0};
    uint32_t expected[4] = {0x88000100, 0x00000001, 0x00000200, 0x00402000};
    z = Salsa20::quarterRound(y);
    
    for (int i = 0; i < 4; i++) {
        ASSERT_EQ(expected[i], z[i]);
    }
    
    delete[] z;
}

TEST(Salsa20_quarterRound, 004) {
    uint32_t* z;
    uint32_t y[4] = {0, 0, 1, 0};
    uint32_t expected[4] = {0x80040000, 0x00000000, 0x00000001, 0x00002000};
    z = Salsa20::quarterRound(y);
    
    for (int i = 0; i < 4; i++) {
        ASSERT_EQ(expected[i], z[i]);
    }
    
    delete[] z;
}

TEST(Salsa20_quarterRound, 005) {
    uint32_t* z;
    uint32_t y[4] = {0, 0, 0, 1};
    uint32_t expected[4] = {0x00048044, 0x00000080, 0x00010000, 0x20100001};
    z = Salsa20::quarterRound(y);
    
    for (int i = 0; i < 4; i++) {
        ASSERT_EQ(expected[i], z[i]);
    }
    
    delete[] z;
}

TEST(Salsa20_quarterRound, 006) {
    uint32_t* z;
    uint32_t y[4] = {0xe7e8c006, 0xc4f9417d, 0x6479b4b2, 0x68c67137};
    uint32_t expected[4] = {0xe876d72b, 0x9361dfd5, 0xf1460244, 0x948541a3};
    z = Salsa20::quarterRound(y);
    
    for (int i = 0; i < 4; i++) {
        ASSERT_EQ(expected[i], z[i]);
    }
    
    delete[] z;
}

TEST(Salsa20_quarterRound, 007) {
    uint32_t* z;
    uint32_t y[4] = {0xd3917c5b, 0x55f1c407, 0x52a58a7a, 0x8f887a3b};
    uint32_t expected[4] = {0x3e2f308c, 0xd90a8f36, 0x6ab2a923, 0x2883524c};
    z = Salsa20::quarterRound(y);
    
    for (int i = 0; i < 4; i++) {
        ASSERT_EQ(expected[i], z[i]);
    }
    
    delete[] z;
}

TEST(Salsa20_rowRound, 001) {
    uint32_t* z;
    uint32_t y[16] = {1, 0, 0, 0,
                          1, 0, 0, 0,
                          1, 0, 0, 0,
                          1, 0, 0, 0};
    uint32_t expected[16] = {0x08008145, 0x00000080, 0x00010200, 0x20500000,
                                 0x20100001, 0x00048044, 0x00000080, 0x00010000,
                                 0x00000001, 0x00002000, 0x80040000, 0x00000000,
                                 0x00000001, 0x00000200, 0x00402000, 0x88000100};
    z = Salsa20::rowRound(y);
    
    for (int i = 0; i < 16; i++) {
        ASSERT_EQ(expected[i], z[i]);
    }
    
    delete[] z;
}

TEST(Salsa20_rowRound, 002) {
    uint32_t* z;
    uint32_t y[16] = {0x08521bd6, 0x1fe88837, 0xbb2aa576, 0x3aa26365,
                          0xc54c6a5b, 0x2fc74c2f, 0x6dd39cc3, 0xda0a64f6,
                          0x90a2f23d, 0x067f95a6, 0x06b35f61, 0x41e4732e,
                          0xe859c100, 0xea4d84b7, 0x0f619bff, 0xbc6e965a};
    uint32_t expected[16] = {0xa890d39d, 0x65d71596, 0xe9487daa, 0xc8ca6a86,
                                0x949d2192, 0x764b7754, 0xe408d9b9, 0x7a41b4d1,
                                0x3402e183, 0x3c3af432, 0x50669f96, 0xd89ef0a8,
                                0x0040ede5, 0xb545fbce, 0xd257ed4f, 0x1818882d};
    z = Salsa20::rowRound(y);
    
    for (int i = 0; i < 16; i++) {
        ASSERT_EQ(expected[i], z[i]);
    }
    
    delete[] z;
}

TEST(Salsa20_columnRound, 001) {
    uint32_t* y;
    uint32_t x[16] = {1, 0, 0, 0,
                          1, 0, 0, 0,
                          1, 0, 0, 0,
                          1, 0, 0, 0};
    uint32_t expected[16] = {0x10090288, 0x00000000, 0x00000000, 0x00000000,
                                 0x00000101, 0x00000000, 0x00000000, 0x00000000,
                                 0x00020401, 0x00000000, 0x00000000, 0x00000000,
                                 0x40a04001, 0x00000000, 0x00000000, 0x00000000};
    y = Salsa20::columnRound(x);
    
    for (int i = 0; i < 16; i++) {
        ASSERT_EQ(expected[i], y[i]);
    }
    
    delete[] y;
}

TEST(Salsa20_columnRound, 002) {
    uint32_t* y;
    uint32_t x[16] = {0x08521bd6, 0x1fe88837, 0xbb2aa576, 0x3aa26365,
                          0xc54c6a5b, 0x2fc74c2f, 0x6dd39cc3, 0xda0a64f6,
                          0x90a2f23d, 0x067f95a6, 0x06b35f61, 0x41e4732e,
                          0xe859c100, 0xea4d84b7, 0x0f619bff, 0xbc6e965a};
    uint32_t expected[16] = {0x8c9d190a, 0xce8e4c90, 0x1ef8e9d3, 0x1326a71a,
                                 0x90a20123, 0xead3c4f3, 0x63a091a0, 0xf0708d69,
                                 0x789b010c, 0xd195a681, 0xeb7d5504, 0xa774135c,
                                 0x481c2027, 0x53a8e4b5, 0x4c1f89c5, 0x3f78c9c8};
    y = Salsa20::columnRound(x);
    
    for (int i = 0; i < 16; i++) {
        ASSERT_EQ(expected[i], y[i]);
    }
    
    delete[] y;
}

TEST(Salsa20_doubleRound, 001) {
    uint32_t* y;
    uint32_t x[16] = {1, 0, 0, 0,
                          0, 0, 0, 0,
                          0, 0, 0, 0,
                          0, 0, 0, 0};
    uint32_t expected[16] = {0x8186a22d, 0x0040a284, 0x82479210, 0x06929051,
                                 0x08000090, 0x02402200, 0x00004000, 0x00800000,
                                 0x00010200, 0x20400000, 0x08008104, 0x00000000,
                                 0x20500000, 0xa0000040, 0x0008180a, 0x612a8020};
    y = Salsa20::doubleRound(x);
    
    for (int i = 0; i < 16; i++) {
        ASSERT_EQ(expected[i], y[i]);
    }
    
    delete[] y;
}

TEST(Salsa20_doubleRound, 002) {
    uint32_t* y;
    uint32_t x[16] = {0xde501066, 0x6f9eb8f7, 0xe4fbbd9b, 0x454e3f57,
                          0xb75540d3, 0x43e93a4c, 0x3a6f2aa0, 0x726d6b36,
                          0x9243f484, 0x9145d1e8, 0x4fa9d247, 0xdc8dee11,
                          0x054bf545, 0x254dd653, 0xd9421b6d, 0x67b276c1};
    uint32_t expected[16] = {0xccaaf672, 0x23d960f7, 0x9153e63a, 0xcd9a60d0,
                                 0x50440492, 0xf07cad19, 0xae344aa0, 0xdf4cfdfc,
                                 0xca531c29, 0x8e7943db, 0xac1680cd, 0xd503ca00,
                                 0xa74b2ad6, 0xbc331c5c, 0x1dda24c7, 0xee928277};
    y = Salsa20::doubleRound(x);
    
    for (int i = 0; i < 16; i++) {
        ASSERT_EQ(expected[i], y[i]);
    }
    
    delete[] y;
}

TEST(Salsa20_littleEndian, 001) {
    uint8_t b[4] = {0, 0, 0, 0};
    ASSERT_EQ(0, Salsa20::littleEndian(b));
}

TEST(Salsa20_littleEndian, 002) {
    uint8_t b[4] = {86, 75, 30, 9};
    ASSERT_EQ(0x091e4b56, Salsa20::littleEndian(b));
}

TEST(Salsa20_littleEndian, 003) {
    uint8_t b[4] = {255, 255, 255, 250};
    ASSERT_EQ(0xfaffffff, Salsa20::littleEndian(b));
}

TEST(Salsa20_littleEndianInverse, 001) {
    uint8_t b[4] = {0, 0, 0, 0};
    uint8_t* b_result;
    b_result = Salsa20::littleEndianInverse(Salsa20::littleEndian(b));
    
    for (int i = 0; i < 4; i++) {
        ASSERT_EQ(b[i], b_result[i]);
    }
    
    delete[] b_result;
}

TEST(Salsa20_littleEndianInverse, 002) {
    uint8_t b[4] = {86, 75, 30, 9};
    uint8_t* b_result;
    b_result = Salsa20::littleEndianInverse(Salsa20::littleEndian(b));
    
    for (int i = 0; i < 4; i++) {
        ASSERT_EQ(b[i], b_result[i]);
    }
    
    delete[] b_result;
}

TEST(Salsa20_littleEndianInverse, 003) {
    uint8_t b[4] = {255, 255, 255, 250};
    uint8_t* b_result;
    b_result = Salsa20::littleEndianInverse(Salsa20::littleEndian(b));
    
    for (int i = 0; i < 4; i++) {
        ASSERT_EQ(b[i], b_result[i]);
    }
    
    delete[] b_result;
}

TEST(Salsa20_hash20, 001) {
    uint8_t* y;
    uint8_t x[64] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    uint8_t expected[64] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    y = Salsa20::hash20(x);
    
    for (int i = 0; i < 64; i++) {
        ASSERT_EQ(expected[i], y[i]);
    }
    
    delete[] y;
}

TEST(Salsa20_hash20, 002) {
    uint8_t* y;
    uint8_t x[64] = {211,159, 13,115, 76, 55, 82,183, 3,117,222, 37,191,187,234,136,
                     49,237,179, 48, 1,106,178,219,175,199,166, 48, 86, 16,179,207,
                     31,240, 32, 63, 15, 83, 93,161,116,147, 48,113,238, 55,204, 36,
                     79,201,235, 79, 3, 81,156, 47,203, 26,244,243, 88,118,104, 54};
    uint8_t expected[64] = {109, 42,178,168,156,240,248,238,168,196,190,203, 26,110,170,154,
                            29, 29,150, 26,150, 30,235,249,190,163,251, 48, 69,144, 51, 57,
                            118, 40,152,157,180, 57, 27, 94,107, 42,236, 35, 27,111,114,114,
                            219,236,232,135,111,155,110, 18, 24,232, 95,158,179, 19, 48,202};
    y = Salsa20::hash20(x);
    
    for (int i = 0; i < 64; i++) {
        ASSERT_EQ(expected[i], y[i]);
    }
    
    delete[] y;
}

TEST(Salsa20_hash20, 003) {
    uint8_t* y;
    uint8_t x[64] = {88,118,104, 54, 79,201,235, 79, 3, 81,156, 47,203, 26,244,243,
                     191,187,234,136,211,159, 13,115, 76, 55, 82,183, 3,117,222, 37,
                     86, 16,179,207, 49,237,179, 48, 1,106,178,219,175,199,166, 48,
                     238, 55,204, 36, 31,240, 32, 63, 15, 83, 93,161,116,147, 48,113};
    uint8_t expected[64] = {179, 19, 48,202,219,236,232,135,111,155,110, 18, 24,232, 95,158,
                            26,110,170,154,109, 42,178,168,156,240,248,238,168,196,190,203,
                            69,144, 51, 57, 29, 29,150, 26,150, 30,235,249,190,163,251, 48,
                            27,111,114,114,118, 40,152,157,180, 57, 27, 94,107, 42,236, 35};
    y = Salsa20::hash20(x);
    
    for (int i = 0; i < 64; i++) {
        ASSERT_EQ(expected[i], y[i]);
    }
    
    delete[] y;
}

TEST(Salsa20_expand20, 001) {
    uint8_t* y;
    uint8_t k[32] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216};
    uint8_t n[16] = {101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116};
    uint8_t expected[64] = {69, 37, 68, 39, 41, 15,107,193,255,139,122, 6,170,233,217, 98,
                            89,144,182,106, 21, 51,200, 65,239, 49,222, 34,215,114, 40,126,
                            104,197, 7,225,197,153, 31, 2,102, 78, 76,176, 84,245,246,184,
                            177,160,133,130, 6, 72,149,119,192,195,132,236,234,103,246, 74};
    
    y = Salsa20::expand20(k, n, true);
    
    for (int i = 0; i < 64; i++) {
        ASSERT_EQ(expected[i], y[i]);
    }
    
    delete[] y;
}

TEST(Salsa20_expand20, 002) {
    uint8_t* y;
    uint8_t k[16] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
    uint8_t n[16] = {101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116};
    uint8_t expected[64] = {39,173, 46,248, 30,200, 82, 17, 48, 67,254,239, 37, 18, 13,247,
                            241,200, 61,144, 10, 55, 50,185, 6, 47,246,253,143, 86,187,225,
                            134, 85,110,246,161,163, 43,235,231, 94,171, 51,145,214,112, 29,
                            14,232, 5, 16,151,140,183,141,171, 9,122,181,104,182,177,193};
    
    y = Salsa20::expand20(k, n, false);
    
    for (int i = 0; i < 64; i++) {
        ASSERT_EQ(expected[i], y[i]);
    }
    
    delete[] y;
}

TEST(Salsa20_encryptAndDecrypt, 001) {
    uint8_t key[32] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216};
    uint8_t nonce[8] = {101,102,103,104,105,106,107,108};
    uint8_t message[32] = "Key de 32 bytes.NADANADANADANAD";
    
    uint8_t* encrypted = Salsa20::encrypt(key, nonce, true, message, 32);
    uint8_t* decrypted = Salsa20::decrypt(key, nonce, true, encrypted, 32);
    
    for (int i = 0; i < 32; i++) {
        ASSERT_EQ(message[i], decrypted[i]);
    }
    
    delete[] encrypted;
    delete[] decrypted;
}

TEST(Salsa20_encryptAndDecrypt, 002) {
    uint8_t key[16] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
    uint8_t nonce[8] = {101,102,103,104,105,106,107,108};
    uint8_t message[32] = "Key de 16 bytes.LOLZLOLZLOLZLOL";
    
    uint8_t* encrypted = Salsa20::encrypt(key, nonce, false, message, 32);
    uint8_t* decrypted = Salsa20::decrypt(key, nonce, false, encrypted, 32);
    
    for (int i = 0; i < 32; i++) {
        ASSERT_EQ(message[i], decrypted[i]);
    }
    
    delete[] encrypted;
    delete[] decrypted;
}

TEST(Salsa20_encryptAndDecrypt, 003) {
    uint8_t key[16] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
    uint8_t nonce[8] = {101,102,103,104,105,106,107,108};
    uint8_t message[400] = "Mensaje de 400 caracteres que no se con que llenar asi que escribo"
    "todo este texto sin sentido que no sirve para NADANADANADANADANADANADANADANADANADANADANADA"
    "NADANADANADANADANADANADANADANADANADANADANADANADANADANADANADANADANADANADANADANADANADANADANA"
    "DANADANADANADANADANADANADANADANADANADANADANADANADANADANADANADANADANADANADANADANADANADANADA"
    "DANADANADANADANADANADASegun el IDE estos son 400 caracteresGGEZ";
    
    uint8_t* encrypted = Salsa20::encrypt(key, nonce, false, message, 400);
    uint8_t* decrypted = Salsa20::decrypt(key, nonce, false, encrypted, 400);
    
    for (int i = 0; i < 400; i++) {
        ASSERT_EQ(message[i], decrypted[i]);
    }
    
    delete[] encrypted;
    delete[] decrypted;
}
