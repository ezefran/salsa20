#include <gtest/gtest.h>
#include <Utils.h>

TEST(Utils_rotateLeft32, RotateThreeTimes) {
	ASSERT_EQ(0b1000, rotateLeft32(0b1, 3));
}

TEST(Utils_rotateLeft32, CircularShift) {
    ASSERT_EQ(0b1, rotateLeft32(0b10000000000000000000000000000000, 1));
}

TEST(Utils_rotateLeft32, TwoCircularShifts) {
    ASSERT_EQ(0b1, rotateLeft32(0b1, 64));
}
