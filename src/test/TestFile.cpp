#include <gtest/gtest.h>
#include <Salsa20.h>
#include <File.h>
#include <Utils.h>

#define TEST_DIR "Para que el IDE no se queje"
#ifdef D_TEST_DIR
    #define TEST_DIR D_TEST_DIR
#endif

void makeDir(std::string path) {
    int err = 0;
    #if defined(WIN32) || defined(_WIN32)
        err = _mkdir(path.c_str());
    #else
        err = mkdir(path.c_str(), S_IRUSR | S_IWUSR);
    #endif
    if (err != 0) {
        if (errno == EEXIST) {
            return;
        } else {
            throw std::runtime_error("No se pudo crear la carpeta " + path);
        }
    }
}

TEST(File_encryptAndDecrypt, 001) {
    // Encripta y desencripta imagen BMP
    uint8_t key[16] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
    uint8_t nonce[8] = {101,102,103,104,105,106,107,108};
    makeDir(std::string(TEST_DIR) + "/gen");
    makeDir(std::string(TEST_DIR) + "/gen/File_encryptAndDecrypt");
    
    File file(std::string(TEST_DIR) + "/res/File_encryptAndDecrypt/test001.bmp", 54);
    char* before = new char[file.getSize()];
    copyArray(file.getBytes(), before, 0, file.getSize());
    
    file.encrypt(key, nonce, false);
    file.save(std::string(TEST_DIR) + "/gen/File_encryptAndDecrypt/test001_1.bmp");
    char* encrypted = new char[file.getSize()];
    copyArray(file.getBytes(), encrypted, 0, file.getSize());
    
    file.decrypt(key, nonce, false);
    file.save(std::string(TEST_DIR) + "/gen/File_encryptAndDecrypt/test001_2.bmp");
    
    for (int i = 0; i < file.getSize(); i++) {
        if (i >= file.getHeaderSize()) {
            SCOPED_TRACE("Body");
            ASSERT_EQ(before[i], file.getBytes()[i]);
        } else {
            SCOPED_TRACE("Header");
            ASSERT_EQ(before[i], encrypted[i]);
        }
    }
        
    delete[] before;
    delete[] encrypted;
}

TEST(File_encryptAndDecrypt, 002) {
    // Encripta una imagen BMP, la altera y la desencripta
    uint8_t key[16] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
    uint8_t nonce[8] = {101,102,103,104,105,106,107,108};
    makeDir(std::string(TEST_DIR) + "/gen");
    makeDir(std::string(TEST_DIR) + "/gen/File_encryptAndDecrypt");
    
    File file(std::string(TEST_DIR) + "/res/File_encryptAndDecrypt/test001.bmp", 54);
    char* before = new char[file.getSize()];
    copyArray(file.getBytes(), before, 0, file.getSize());
    
    file.encrypt(key, nonce, false);
    file.alter();
    file.save(std::string(TEST_DIR) + "/gen/File_encryptAndDecrypt/test002_1.bmp");
    
    file.decrypt(key, nonce, false);
    file.save(std::string(TEST_DIR) + "/gen/File_encryptAndDecrypt/test002_2.bmp");
    char* encrypted = new char[file.getSize()];
    copyArray(file.getBytes(), encrypted, 0, file.getSize());
        
    delete[] before;
    delete[] encrypted;
}

TEST(File_encryptAndDecrypt, 003) {
    // Desencripta una imagen BMP y la encripta de nuevo
    uint8_t key[16] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16};
    uint8_t nonce[8] = {101,102,103,104,105,106,107,108};
    makeDir(std::string(TEST_DIR) + "/gen");
    makeDir(std::string(TEST_DIR) + "/gen/File_encryptAndDecrypt");
    
    File file(std::string(TEST_DIR) + "/gen/File_encryptAndDecrypt/test001_1.bmp", 54);
    char* before = new char[file.getSize()];
    copyArray(file.getBytes(), before, 0, file.getSize());
    
    file.encrypt(key, nonce, false);
    file.save(std::string(TEST_DIR) + "/gen/File_encryptAndDecrypt/test003_1.bmp");
    char* encrypted = new char[file.getSize()];
    copyArray(file.getBytes(), encrypted, 0, file.getSize());
    
    file.decrypt(key, nonce, false);
    file.save(std::string(TEST_DIR) + "/gen/File_encryptAndDecrypt/test003_2.bmp");
    
    for (int i = 0; i < file.getSize(); i++) {
        if (i >= file.getHeaderSize()) {
            SCOPED_TRACE("Body");
            ASSERT_EQ(before[i], file.getBytes()[i]);
        } else {
            SCOPED_TRACE("Header");
            ASSERT_EQ(before[i], encrypted[i]);
        }
    }
        
    delete[] before;
    delete[] encrypted;
}

TEST(File_XOR, 001) {
    // Encriptar dos imagenes con el mismo nonce y hacer XOR entre los datos encriptados
    uint8_t key[32] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216};
    uint8_t nonce[8] = {101,102,103,104,105,106,107,108};
    makeDir(std::string(TEST_DIR) + "/gen");
    makeDir(std::string(TEST_DIR) + "/gen/FILE_XOR");
        
    File img1(std::string(TEST_DIR) + "/res/FILE_XOR/test001_linux.bmp", 54); // M1
    img1.encrypt(key, nonce, true); // C1
    
    File img2(std::string(TEST_DIR) + "/res/FILE_XOR/test001_win.bmp", 54); // M2
    img2.encrypt(key, nonce, true); // C2
    
    File* res = img1.XOR(&img2); // C1 XOR C2 == M1 XOR M2
    ASSERT_TRUE(res);
    res->save(std::string(TEST_DIR) + "/gen/FILE_XOR/test001_c1XORc2.bmp");
    
    img1.decrypt(key, nonce, true); // M1
    img2.decrypt(key, nonce, true); // M2
    
    res = res->XOR(&img1); // C1 XOR C2 XOR M1 == M1 XOR M2 XOR M1 == M2
    ASSERT_TRUE(res);
    res->save(std::string(TEST_DIR) + "/gen/FILE_XOR/test001_resXORm1.bmp");
    
    for (int i = res->getHeaderSize(); i < res->getSize(); i++) {
        ASSERT_EQ(img2.getBytes()[i], res->getBytes()[i]);
    }
    
    delete res;
}
