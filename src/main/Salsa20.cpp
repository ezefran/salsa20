#include "Salsa20.h"
#include "include/Utils.h"
#include <stddef.h>

Salsa20::Salsa20() {
	// TODO Auto-generated constructor stub
}

uint32_t* Salsa20::quarterRound(uint32_t* y) {
	uint32_t* z = new uint32_t[4];

	z[1] = y[1] ^ (rotateLeft32(y[0] + y[3], 7));
	z[2] = y[2] ^ (rotateLeft32(z[1] + y[0], 9));
	z[3] = y[3] ^ (rotateLeft32(z[2] + z[1], 13));
	z[0] = y[0] ^ (rotateLeft32(z[3] + z[2], 18));

	return z;
}

uint32_t* Salsa20::rowRound(uint32_t* y) {
    uint32_t* p_aux[4];
    uint32_t* z = new uint32_t[16];
    
    if (verbose) LOG(std::string("Antes de Rowound:\n").append(matrixToString(y, 4, 4)));
    
    uint32_t m_aux[4][4];
    m_aux[0][0] = y[0]; m_aux[0][1] = y[1]; m_aux[0][2] = y[2], m_aux[0][3] = y[3];
    m_aux[1][0] = y[5]; m_aux[1][1] = y[6]; m_aux[1][2] = y[7], m_aux[1][3] = y[4];
    m_aux[2][0] = y[10]; m_aux[2][1] = y[11]; m_aux[2][2] = y[8], m_aux[2][3] = y[9];
    m_aux[3][0] = y[15]; m_aux[3][1] = y[12]; m_aux[3][2] = y[13], m_aux[3][3] = y[14];
    
    // esto podria hacerse en paralelo:
    for (int i = 0; i < 4; i++) {
        p_aux[i] = quarterRound(m_aux[i]);
    }
    
    z[0] = p_aux[0][0]; z[1] = p_aux[0][1]; z[2] = p_aux[0][2]; z[3] = p_aux[0][3];
    z[4] = p_aux[1][3]; z[5] = p_aux[1][0]; z[6] = p_aux[1][1]; z[7] = p_aux[1][2];
    z[8] = p_aux[2][2]; z[9] = p_aux[2][3]; z[10] = p_aux[2][0]; z[11] = p_aux[2][1];
    z[12] = p_aux[3][1]; z[13] = p_aux[3][2]; z[14] = p_aux[3][3]; z[15] = p_aux[3][0];
    
    if (verbose) LOG(std::string("Despues de Rowround:\n").append(matrixToString(*p_aux, 4, 4)));
    
    for (int i = 0; i < 4; i++) {
        delete[] p_aux[i];
    }
    
    return z;
}

uint32_t* Salsa20::columnRound(uint32_t* x) {
    uint32_t* p_aux[4];
    uint32_t* y = new uint32_t[16];
    
    if (verbose) LOG(std::string("Antes de Columnround:\n").append(matrixToString(x, 4, 4)));
    
    uint32_t m_aux[4][4];
    m_aux[0][0] = x[0]; m_aux[0][1] = x[4]; m_aux[0][2] = x[8], m_aux[0][3] = x[12];
    m_aux[1][0] = x[5]; m_aux[1][1] = x[9]; m_aux[1][2] = x[13], m_aux[1][3] = x[1];
    m_aux[2][0] = x[10]; m_aux[2][1] = x[14]; m_aux[2][2] = x[2], m_aux[2][3] = x[6];
    m_aux[3][0] = x[15]; m_aux[3][1] = x[3]; m_aux[3][2] = x[7], m_aux[3][3] = x[11];
    
    // esto podria hacerse en paralelo:
    for (int i = 0; i < 4; i++) {
        p_aux[i] = quarterRound(m_aux[i]);
    }
    
    y[0] = p_aux[0][0]; y[1] = p_aux[1][3]; y[2] = p_aux[2][2]; y[3] = p_aux[3][1];
    y[4] = p_aux[0][1]; y[5] = p_aux[1][0]; y[6] = p_aux[2][3]; y[7] = p_aux[3][2];
    y[8] = p_aux[0][2]; y[9] = p_aux[1][1]; y[10] = p_aux[2][0]; y[11] = p_aux[3][3];
    y[12] = p_aux[0][3]; y[13] = p_aux[1][2]; y[14] = p_aux[2][1]; y[15] = p_aux[3][0];
    
    for (int i = 0; i < 4; i++) {
        delete[] p_aux[i];
    }
    
    if (verbose) LOG(std::string("Despues de Columnround:\n").append(matrixToString(y, 4, 4)));
    
    return y;
}

uint32_t* Salsa20::doubleRound(uint32_t* x) {
    return rowRound(columnRound(x));
}

uint32_t Salsa20::littleEndian(uint8_t* b) {
    return b[0] + (1 << 8) * b[1] + (1 << 16) * b[2] + (1 << 24) * b[3];
}

uint8_t* Salsa20::littleEndianInverse(uint32_t w) {
    uint8_t* b = new uint8_t[4];
    b[0] = w & 0xFF;
    b[1] = (w >> 8) & 0xFF;
    b[2] = (w >> 16) & 0xFF;
    b[3] = (w >> 24) & 0xFF;
    return b;
}

uint8_t* Salsa20::littleEndianInverse(uint8_t *b, uint32_t *w) {
    b[0] = *w & 0xFF;
    b[1] = (*w >> 8) & 0xFF;
    b[2] = (*w >> 16) & 0xFF;
    b[3] = (*w >> 24) & 0xFF;
}

uint8_t* Salsa20::hash20(uint8_t* x) {
    uint8_t* y = new uint8_t[64];
    uint32_t* x32 = new uint32_t[16];
    uint32_t* z32 = new uint32_t[16];
    
    for (int i = 0; i < 16; i++) {
        x32[i] = littleEndian(x + 4*i);
    }
    
    z32 = doubleRound(x32);
    for (int i = 1; i < 10; i++) {
        z32 = doubleRound(z32);
    }
    
    for (int i = 0; i < 16; i++) {
        uint8_t* aux = littleEndianInverse(z32[i] + x32[i]);
        y[4*i] = aux[0];
        y[4*i + 1] = aux[1];
        y[4*i + 2] = aux[2];
        y[4*i + 3] = aux[3];
        delete[] aux;
    }
    
    if (verbose) {
        uint32_t suma[16];
        for (int i = 0; i < 16; i++) {
            suma[i] = z32[i] + x32[i];
        }
        LOG(std::string("Bloque original (x):\n").append(matrixToString(x32, 4, 4)));
        LOG(std::string("Bloque a sumar (Salsa20/20(x)):\n").append(matrixToString(z32, 4, 4)));
        LOG(std::string("Bloque hasheado (x + Salsa20/20(x)):\n").append(matrixToString(suma, 4, 4)));
    }
    
    delete[] x32;
    delete[] z32;
    
    return y;
}

uint8_t* Salsa20::expand20(uint8_t* k, uint8_t* n, bool is32Bytes) {
    uint8_t* x = new uint8_t[64];
    
    // constantes (16)
    x[0] = 'e'; x[1] = 'x'; x[2] = 'p'; x[3] = 'a'; x[20] = 'n'; x[21] = 'd';
    x[22] = ' '; x[23] = '3'; x[40] = '2'; x[41] = '-';
    x[42] = 'b'; x[43] = 'y'; x[60] = 't'; x[61] = 'e'; x[62] = ' '; x[63] = 'k';
    
    if (! is32Bytes) {
        x[23] = '1'; x[40] = '6';
    }
    
    // clave k0(16)
    for (int i = 0; i < 16; i++) {
        x[i + 4] = k[i];
    }
    
    // nonce(8) + block-counter(8)
    for (int i = 0; i < 16; i++) {
        x[i + 24] = n[i];
    }
    
    // clave k1(16)
    if (is32Bytes) {
        for (int i = 0; i < 16; i++) {
                x[i + 44] = k[i + 16];
        }
    } else {
        for (int i = 0; i < 16; i++) {
                x[i + 44] = k[i];
        }
    }
        
    return hash20(x);
}

uint8_t* Salsa20::encrypt(uint8_t* key, uint8_t* nonce, bool keyIs32Bytes, uint8_t* message, int bytes) {
    uint32_t aux = 0;
    uint8_t v[16];
    uint8_t* encrypted = new uint8_t[bytes];
    
    copyArray(nonce, v, 0, 8);
    for(int i = 8; i < 16; i++) {
        v[i] = 0;
    }
    
    int j = 0;
    while (true) {
        // esto podria hacerse en paralelo:
        uint8_t* hashed = expand20(key, v, keyIs32Bytes);
        
        aux = littleEndian(v + 8) + 1;
        littleEndianInverse(v + 8, &aux);
        if (aux == 0) {
            aux = littleEndian(v + 8 + 4) + 1;
            littleEndianInverse(v + 8 + 4, &aux);
        }
        
        for (int i = 0; i < 64 && i < bytes; i++) {
            encrypted[i + 64*j] = message[i + 64*j] ^ hashed[i];
        }
        
        if (bytes <= 64) return encrypted;
        
        bytes -= 64;
        j++;
    }
}

uint8_t* Salsa20::decrypt ( uint8_t* key, uint8_t* nonce, bool keyIs32Bytes, uint8_t* message, int bytes ) {
    return encrypt(key, nonce, keyIs32Bytes, message, bytes);
}

Salsa20::~Salsa20() {
	// TODO Auto-generated destructor stub
}
