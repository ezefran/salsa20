#include "File.h"
#include "Salsa20.h"
#include "include/Utils.h"
#include <iostream>
#include <string.h>

int main(int argc, char *argv[]) {
    // Validacion muy simple de input
    if (argc != 3 && argc != 4) {
        std::cout << "Numero incorrecto de argumentos" << std::endl;
        return 1;
    }
    if (argc == 4) {
        if (strcmp(argv[3], "-v") == 0) {
            verbose = true;
        } else {
            std::cout << "Opcion invalida" << std::endl;
            return 1;
        }
    }
    // clave y nonce hardcodeados
    uint8_t key[16] = {1,0,0,0,1,0,0,0,1,0,0,0,1,0,0,0};
    uint8_t nonce[8] = {2,0,0,0,2,0,0,0};
    
    // cambiar el tamanio de header segun el tipo de archivo
    File f(argv[1], 54);
    f.encrypt(key, nonce, false);
    f.save(argv[2]);
        
    return 0;
}
