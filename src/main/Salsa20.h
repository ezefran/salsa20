#ifndef SALSA20_H_
#define SALSA20_H_

#include <stdint.h>

class Salsa20 {
    private:
        Salsa20();
	public:
		virtual ~Salsa20();
		static uint32_t* quarterRound(uint32_t* y);
		static uint32_t* rowRound(uint32_t* y);
		static uint32_t* columnRound(uint32_t* x);
		static uint32_t* doubleRound(uint32_t* x);
		static uint32_t littleEndian(uint8_t* b);
        static uint8_t* littleEndianInverse(uint32_t w);
        static uint8_t* littleEndianInverse(uint8_t* b, uint32_t *w);
		static uint8_t* hash20(uint8_t* x);
		static uint8_t* expand20(uint8_t* k, uint8_t* n, bool is32Bytes);
		static uint8_t* encrypt(uint8_t* key, uint8_t* nonce, bool keyIs32Bytes, uint8_t* message, int bytes);
		static uint8_t* decrypt(uint8_t* key, uint8_t* nonce, bool keyIs32Bytes, uint8_t* message, int bytes);
};
#endif
