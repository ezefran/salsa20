#include "File.h"
#include "Salsa20.h"
#include "include/Utils.h"

File::File(std::string fileName, size_t headerSize) {
    this->headerSize = headerSize;
	std::ifstream file(fileName, std::ios_base::binary);
    if (file) {
        file.seekg(0, file.end);
        this->bytesSize = file.tellg();
        file.seekg(0, file.beg);
        
        this->bytes = new char[this->bytesSize];
        file.read(this->bytes, this->bytesSize);
        file.close();
    } else {
        throw std::runtime_error("El archivo no existe.");
        exit(EXIT_FAILURE);
    }
}

File::File(char* bytes, size_t size,size_t headerSize) {
    this->bytes = bytes;
    this->bytesSize = size;
    this->headerSize = headerSize;
}

void File::encrypt(uint8_t* key, uint8_t* nonce, bool keyIs32Bytes) {
    char* res = (char*) Salsa20::encrypt(key, nonce, keyIs32Bytes, (uint8_t*) this->bytes + this->headerSize, this->bytesSize);
    copyArray(res, this->bytes, this->headerSize, this->bytesSize);
}

void File::decrypt(uint8_t* key, uint8_t* nonce, bool keyIs32Bytes) {
    this->encrypt(key, nonce, keyIs32Bytes);
}

void File::save(std::string fileName) {
    std::ofstream of(fileName, std::ios_base::binary);
    of.write(this->bytes, this->bytesSize);
    of.close();
}

void File::alter() {
    int s = this->getSize();
    for (int i = s/2 - s/4; i < s/2 + s/4; i++) {
        if (i % 2 == 0) this->getBytes()[i] = 0;
    }
}

File* File::XOR(File* f) {
    char* r;
    if (this->getHeaderSize() == f->getHeaderSize() && this->getSize() >= f->getSize()) {
        r = new char[this->getSize()];
        for (int i = 0; i < f->getHeaderSize(); i++) {
            r[i] = f->getBytes()[i];
        }
        for (int i = this->getHeaderSize(); i < f->getSize(); i++) {
            r[i] = this->getBytes()[i] ^ f->getBytes()[i];
        }
    }
    else if (this->getHeaderSize() == f->getHeaderSize() && this->getSize() < f->getSize()) {
        r = new char[f->getSize()];
        for (int i = 0; i < this->getHeaderSize(); i++) {
            r[i] = this->getBytes()[i];
        }
        for (int i = f->getHeaderSize(); i < this->getSize(); i++) {
            r[i] = f->getBytes()[i] ^ this->getBytes()[i];
        }
    }
    else {
        return NULL;
    }
    return new File(r, this->getSize(), this->getHeaderSize());
}


char* File::getBytes() {
    return this->bytes;
}

int File::getSize() {
    return this->bytesSize;
}

int File::getHeaderSize() {
    return this->headerSize;
}

File::~File() {
	delete[] bytes;
}
