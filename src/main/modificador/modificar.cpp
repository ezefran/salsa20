#include "File.h"
#include <iostream>
#include <string.h>

int main(int argc, char *argv[]) {
    // Validacion muy simple de input
    if (argc != 3) {
        std::cout << "Numero incorrecto de argumentos" << std::endl;
        return 1;
    }
    
    File f(argv[1], 54);
    f.alter();
    f.save(argv[2]);

    return 0;
}
