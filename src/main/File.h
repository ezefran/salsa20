#ifndef FILE_H_
#define FILE_H_

#include <stdint.h>
#include <string>
#include <fstream>

class File {
    private:
        char* bytes;
        size_t bytesSize;
        size_t headerSize;
    public:        
        File(std::string fileName, size_t headerSize);
        File(char* bytes, size_t size,size_t headerSize);
        virtual ~File();
        void encrypt(uint8_t* key, uint8_t* nonce, bool keyIs32Bytes);
        void decrypt(uint8_t* key, uint8_t* nonce, bool keyIs32Bytes);
        void save(std::string fileName);
        File* XOR(File *f);
        char* getBytes();
        int getSize();
        int getHeaderSize();
        void alter();
};
#endif
