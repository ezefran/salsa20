#ifndef INCLUDE_UTILS_H_
#define INCLUDE_UTILS_H_

#include <stddef.h>
#include <sstream>
#include <string>
#include <iomanip>

extern bool verbose;

void LOG(std::string str);

inline uint32_t rotateLeft32(uint32_t i, int n) {
	return (i << n) | (i >> (32 - n));
}

template<typename T>
inline void copyArray(T* from, T* to, int offset, size_t max) {
    for (int i = 0; i < max; i++) {
        to[i + offset] = from[i];
    }
}

inline std::string matrixToString(uint32_t* m, int rows, int columns) {
    std::ostringstream ss;
    for (int i = 0; i < rows; i++) {
        for (int j = 0; j < columns; j++) {
            ss << "0x" << std::uppercase << std::hex  << std::setw(8) << std::setfill('0') << m[i*rows + j] << "\t" << std::nouppercase;
        }
        ss << std::endl;
    }
    return ss.str();
}

#endif
