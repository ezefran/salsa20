#include "Utils.h"
#include <iostream>

bool verbose = false;
int count = 0;

// Solo sirve para visualizar los cambios en la matriz para el primer bloque
void LOG(std::string str) {
    if (count > 40 + 2) return;
    if (verbose) {
        if (count % 2 == 0 || count >= 39) {
            std::cout << str << std::endl;
        }
    }
    count++;
}
