## Salsa20
Implementación simple del algoritmo de encriptación Salsa20 en C++

### Requisitos
```
SCons
Google Test (Para compilar los tests)
```
### Compilación
Clonar el repositorio y en la carpeta raíz del proyecto ejecutar lo siguiente en la terminal:
```
scons
```
Esto genera los ejecutables en la carpeta `bin`.

### Compilar y ejecutar Tests
Primero se debe compilar Google Test y luego ejecutar lo siguiente en la carpeta raíz del proyecto:
```
scons test
```
### Ejecutar Main
En la carpeta `bin` ejecutar lo siguiente en la terminal:
##### Windows
```
Salsa20.exe <file> <dest>
```
##### Linux
```
./Salsa20 <file> <dest>
```
Donde `file` es el *path* del archivo a encriptar y `dest` es el *path* donde se va a guardar el archivo encriptado. Toma como tamaño de header 54 bytes para encriptar imagenes *bmp*.

### Compilar Google Test
Ejecutar los siguientes comandos en la carpeta raíz del proyecto:
```
git clone https://github.com/google/googletest.git ./gtest
cd ./gtest
mkdir build
cd build
cmake -DBUILD_SHARED_LIBS=ON ..
make
```
